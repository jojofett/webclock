import React from 'react';
import styles from './HandHour.module.css';

var moment = require('moment');

class HandHour extends React.Component {
	constructor(props) {
	  super(props);
	  this.state = {time: moment(props.time.format())};
    }

	refresh() {
	  let minutes = this.state.time.minutes();
	  let hours = this.state.time.hours();
	  let angle = 30 * (hours % 12) + (minutes / 2);
	  
	  let elements = document.querySelectorAll('.' + styles.hours);
	  elements[0].style.webkitTransform = 'rotateZ('+ angle +'deg)';
	  elements[0].style.transform = 'rotateZ('+ angle +'deg)';
    }
	
	componentDidMount() {
		this.refresh();
	}
	
	componentDidUpdate() {
		this.refresh();
	}
	
	static getDerivedStateFromProps(props, state) {
		if (props.time.format() !== state.time.format()) {
		  return {
			time: moment(props.time.format())
		  };
		}

		// Return null if the state hasn't changed
		return null;
	}
	
	render() {
		return(
		  <div className={styles.hoursContainer}>
			<div className={styles.hours}></div>
		  </div>
		);
	}
}

export default HandHour;