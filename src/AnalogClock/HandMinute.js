import React from 'react';
import styles from './HandMinute.module.css';

var moment = require('moment');

class HandMinute extends React.Component {
	
	constructor(props) {
	  super(props)
	  this.state = {time: moment(props.time.format())};
    }

	refresh() {
	  let seconds = this.state.time.seconds();
	  let minutes = this.state.time.minutes();
	  let angle = 6 * minutes + (seconds/60 * 6);

	  let elements = document.querySelectorAll('.' + styles.minutes);
	  elements[0].style.webkitTransform = 'rotateZ('+ angle +'deg)';
	  elements[0].style.transform = 'rotateZ('+ angle +'deg)';
    }
	
	componentDidMount() {
		this.refresh();
	}
	
	componentDidUpdate() {
		this.refresh();
	}
	
	static getDerivedStateFromProps(props, state) {
		if (props.time.format() !== state.time.format()) {
		  return {
			time: moment(props.time.format())
		  };
		}
		
		return null;
	}
	
	render() {
		return(
		  <div className={styles.minutesContainer}>
			<div className={styles.minutes}></div>
		  </div>
		);
	}
}

export default HandMinute;