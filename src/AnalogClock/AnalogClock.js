import React from 'react';
import styles from './AnalogClock.module.css'; 
import HandHour from './HandHour';
import HandMinute from './HandMinute';
import HandSecond from './HandSecond';

var moment = require('moment');

class AnalogClock extends React.Component {
  constructor(props) {
	super(props)
	this.state = {time: moment(props.time.format()), startTime: moment(props.time.format())};
  }
  
  componentDidMount() {
    this.timerID = setInterval(
      () => this.refresh(),
      1000
    );
  }
  
  componentWillUnmount() {
    clearInterval(this.timerID);
  }
  
  refresh() {
    this.setState((state) => {
      return {time: moment(this.state.time.format()).add(1, 's')}
    });
  }
  
  static getDerivedStateFromProps(props, state) {
    if (props.time.format() !== state.startTime.format()) {
      return {
        time: moment(props.time.format()),
		startTime: moment(props.time.format())
      };
    }

    // Return null if the state hasn't changed
    return null;
  }
  
  render() {
    return (
	  <div className={styles.clockContainer}>
	    <article className={styles.clock}>
		  <HandHour time={this.state.time}/>
		  <HandMinute time={this.state.time}/>
		  <HandSecond time={this.state.time}/>
	    </article>
	  </div>
	);
  }
}

export default AnalogClock;