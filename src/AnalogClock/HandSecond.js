import React from 'react';
import styles from './HandSecond.module.css';

var moment = require('moment');

class HandSecond extends React.Component {
	constructor(props) {
	  super(props)
	  this.state = {time: moment(props.time.format())};
    }
 
	refresh() {
	  let seconds = this.state.time.seconds();
	  let angle = 6 * seconds;
	  let elements = document.querySelectorAll('.' + styles.seconds);
	  elements[0].style.webkitTransform = 'rotateZ('+ angle +'deg)';
	  elements[0].style.transform = 'rotateZ('+ angle +'deg)';
    }
	
	componentDidMount() {
		this.refresh();
	}
	
	componentDidUpdate() {
		this.refresh();
	}
	
	static getDerivedStateFromProps(props, state) {
		if (props.time.format() !== state.time.format()) {
		  return {
			time: moment(props.time.format())
		  };
		}
		
		return null;
	}
	
	render() {
		return(
		  <div className={styles.secondsContainer}>
			<div className={styles.seconds}></div>
		  </div>
		);
	}
}

export default HandSecond;