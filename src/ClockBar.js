import React from 'react';
import styles from './ClockBar.module.css'; 
import AnalogClock from './AnalogClock/AnalogClock';
import DigitalClock from './DigitalClock/DigitalClock';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';

const hours = [
  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', 
  '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'
];

const by5 = [
	'0', '5', '10', '15', '20', '25', 
	'30', '35', '40', '45', '50', '55'
];

const defaultOption = hours[0];

var moment = require('moment');

class ClockBar extends React.Component {
	constructor(props) {
		super(props)
		this.onSelectHours = this.onSelectHours.bind(this);
		this.onSelectMinutes = this.onSelectMinutes.bind(this);
		this.onSelectSeconds = this.onSelectSeconds.bind(this);
		this.state = {time: props.time};
	}
	
	onSelectHours(option) {
		this.setState((state) => {
			return {time: moment(this.state.time.format()).hour(option.label)};
		});
	}
	
	onSelectMinutes(option) {
		this.setState((state) => {
			return {time: moment(this.state.time.format()).minutes(option.label)};
		});
	}
	
	onSelectSeconds(option) {
		this.setState((state) => {
			return {time: moment(this.state.time.format()).seconds(option.label)};
		});
	}
	
	render() {
		return (
			<div>
			<div className={styles.startTime}>
				<h2>Hours:</h2>
				<Dropdown options={hours} value={defaultOption} onChange={this.onSelectHours} placeholder="Select an option" />
				<h2>Minutes:</h2>
				<Dropdown options={by5} value={defaultOption} onChange={this.onSelectMinutes} placeholder="Select an option" />
				<h2>Seconds:</h2>
				<Dropdown options={by5} value={defaultOption} onChange={this.onSelectSeconds} placeholder="Select an option" />
			</div>	 
		    <div className={styles.clockBar}>
			  <AnalogClock time={this.state.time} />
			  <DigitalClock time={this.state.time} />
			</div>
			</div>
		);
	}
}

export default ClockBar;