import React from 'react';
import './App.css';
import ClockBar from './ClockBar';

var moment = require('moment');

function App() {
  return (
    <div className="App">
      <header>
	    <link href="https://fonts.googleapis.com/css2?family=Monofett&display=swap" rel="stylesheet" />
      </header>
	  
	  <ClockBar time={moment()}/>
    </div>
  );
}

export default App;
